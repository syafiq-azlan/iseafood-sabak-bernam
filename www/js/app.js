// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
  })

  .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html',
        controller: 'HomeCtrl'
      }
    }
  })

  .state('app.attraction', {
      url: '/attraction',
      views: {
        'menuContent': {
          templateUrl: 'templates/attraction.html'
        }
      }
    })

    .state('app.market', {
      url: '/market',
      views: {
        'menuContent': {
          templateUrl: 'templates/market.html',
          controller: 'MarketCtrl'
        }
      }
    })

    .state('app.select', {
      url: '/select',
      views: {
        'menuContent': {
          templateUrl: 'templates/select.html',
          controller: 'SelectCtrl'
        }
      }
    })

    .state('app.top', {
      url: '/top',
      views: {
        'menuContent': {
          templateUrl: 'templates/top.html',
        }
      }
    })

    .state('app.video', {
      url: '/video',
      views: {
        'menuContent': {
          templateUrl: 'templates/video.html',
          controller : 'VideoCtrl'
        }
      }
    })

    .state('app.about', {
      url: '/about',
      views: {
        'menuContent': {
          templateUrl: 'templates/about.html',
        }
      }
    })

    .state('app.where', {
      url: '/where',
      views: {
        'menuContent': {
          templateUrl: 'templates/where.html',
          controller: 'WhereCtrl'
        }
      }
    })

  .state('app.recipe', {
    url: '/recipe',
    views: {
      'menuContent': {
        templateUrl: 'templates/recipe.html',
        controller: 'RecipeCtrl'
      }
    }
  })

  .state('app.sungai', {
    url: '/sungai',
    views: {
      'menuContent': {
        templateUrl: 'templates/sungai.html',
        controller: 'SungaiCtrl'
      }
    }
  })

  .state('app.sabak', {
    url: '/sabak',
    views: {
      'menuContent': {
        templateUrl: 'templates/sabak.html',
        controller: 'SabakCtrl'
      }
    }
  })

  .state('app.sekinchan', {
    url: '/sekinchan',
    views: {
      'menuContent': {
        templateUrl: 'templates/sekinchan.html',
        controller: 'SekinchanCtrl'
      }
    }
  })

  .state('app.sekinchanRes', {
    url: '/sekinchanRes',
    views: {
      'menuContent': {
        templateUrl: 'templates/sekinchanRes.html',
        controller: 'SekinchanResCtrl'
      }
    }
  })

  .state('app.sungaiRes', {
    url: '/sungaiRes',
    views: {
      'menuContent': {
        templateUrl: 'templates/sungaiRes.html',
        controller: 'SungaiResCtrl'
      }
    }
  })

  .state('app.sabakRes', {
    url: '/sabakRes',
    views: {
      'menuContent': {
        templateUrl: 'templates/sabakRes.html',
        controller: 'SabakResCtrl'
      }
    }
  })

  .state('app.recipeDetails', {
    url: '/recipeDetails',
    views: {
      'menuContent': {
        templateUrl: 'templates/recipeDetails.html',
        controller: 'RecipeDetailsCtrl'
      }
    }
  })


  .state('app.credit', {
    url: '/credit',
    views: {
      'menuContent': {
        templateUrl: 'templates/credits.html',
      }
    }
  })

  .state('app.info', {
    url: '/info',
    views: {
      'menuContent': {
        templateUrl: 'templates/info.html',
      }
    }
  });



  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
});

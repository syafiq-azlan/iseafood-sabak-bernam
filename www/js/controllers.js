angular.module('starter.controllers', [])


.run(function($ionicPlatform, $state, $window, $ionicPopup, $rootScope) {

console.log("runn");

    var items = []

    items.push(
    {
      id           : 1,
      name         : 'Sweet, Sour And Spicy Sea Bass',
      img          : 'rs1.jpg',
      ingredients  : 'One sea bass, Blended dried chili, Red onions, Garlics, Ginger sliced thinly, Spring onion, Green chili, Tomatoes cut into cubes,Oyster sauce, Thai chili sauce, Fish sauce, Tomato sauce, Pineapple cubes, Cucumber cubes, Basil leave, Corn flour mixed into a cup of water, Salt, Sugar, Oil', 
      step1        : '1. Clean the fish and marinate the fish using the blended dried chili, cord flour and salt',
      step2        : '2. Sauté the onions, ginger, spring onion and chili paste',
      step3        : '3. Add the tomatoes, salt, sugar, fish sauce, tomato sauce and corn flour Stir and bring it to a boil. Add more water if necessary',
      step4        : '4. Then add the pineapples, basil leaves and cucumber cubes',
      step5        : '5. Reduce the heat and add the fish',
      step6        : '6. Remove the fish when its cook and add parsley as garnishing before serving',
      step7        : '7. Heat the remaining oil, and then fry the ginger, garlic and chilies for about 2 minutes until golden. ',
      step8        : '8. Take off the heat and toss in the spring onions. ',
      step9        : '9. Splash the fish with a little soy sauce and spoon over the contents of the pan'  
    },
    {
      id           : 2,
      name         : 'Steamed Sea Bass With Ginger',
      img          : 'rs2.jpg',
      ingredients  : '6 sea bass fillets, about 140g/5oz each, skin on and scaled, About 3 table spoon sunflower oil, Large knob of ginger, peeled and shredded into matchsticks, 3 garlic clove, thinly sliced, 3 fat, fresh red chili deseeded and thinly shredded, Spring onion, shredded long-ways, 1 table spoon of soy sauce', 
      step1        : '1. Season the fish with salt and pepper, then slash the skin 3 times',
      step2        : '2. Heat a heavy-based frying pan and add 1 table spoon oil',
      step3        : '3. Once hot, fry the fish, skin-side down, for 5 minutes or until the skin is very crisp and golden',
      step4        : '4. The fish will be almost cooked through. Turn over, cook for another 30 secs-1 min, then transfer to a serving plate and keep warm',
      step5        : '5. Fry the fish in 2 batches'  
    },
    {
      id           : 3,
      name         : 'Grilled Sea Bass',
      img          : 'rs3.jpg',
      ingredients  : '1 medium-sized sea bass, 1 bay leaf, turmeric, finely sliced, 2 lime leaves, finely sliced, Ingredients blended together : 3 shallots, 2 cloves garlic, 1 stalk lemon grass, sliced, 3 cups finely sliced ginger flower, 1 tablespoon chili paste, 1 tablespoon fish curry, 3 tablespoon tamarind water, 1 tablespoon dark soy sauce, Slightly toasted shrimp paste, 1 inch ginger, 1/2 inch galangal, Sugar, salt, A little water, Seasoning powder', 
      step1        : '1. Heat the frying pan',
      step2        : '2. Fry the blended ingredients in sufficient oil and stir until the smell comes out',
      step3        : '3. Turn off the heat',
      step4        : '4. Prepare the foil and the tray for grilling',
      step5        : '5. Place the sea bass in the middle of the foil (can also be wrapped in banana leaves)',
      step6        : '6. Pour all the flavoring on top of the sea bass',
      step7        : '7. Sprinkle sliced lime leaves and turmeric leaf and wrap nicely',
      step8        : '8. Let it sit for 15 minutes',
      step9        : '9. Grill the fish for 30 minutes at 200 degrees celcius',
      step10       : '10. Unwrapped the fish and let it cook for another 15 minutes',
      step11       : '11. Squeeze a little lime juice when serving' 
    },
   {
      id           : 4,
      name         : 'Siput Sedut Masak Lemak Cili Api',
      img          : 'rs4.jpg',
      ingredients  : '800gm siput sedut (wash and exhaust the tip), 3 shallots, 2 cloves garlic, 1 inch turmeric, 5 bird eye chilies (according to liking), 1 cup coconut milk, 1 stock cube anchovies, 1 piece sliced tamarind, 1 stalk lemongrass crushed, Turmeric leaf (sliced thin), Pinch of salt', 
      step1        : '1. Place siput sedut that had been cleaned and tip removed into the pan and pour in the coconut milk',
      step2        : '2. Pound the shallots, garlic, turmeric, chili padi and put the paste in the pan',
      step3        : '3. Put in the anchovies cubes and lemon grass',
      step4        : '4. Then cooked with coconut milk over medium heat so as not to break the oil',
      step5        : '5. Constantly stir slowly so that the milk does not clot. After almost 5 minutes enter the tamarind and season with salt',
      step6        : '6. Cook for 2 minutes then turn off the heat and sprinkle turmeric leaf'  
    },
    {
      id           : 5,
      name         : 'Siput Sedut In Soy Sauce',
      img          : 'rs5.jpg',
      ingredients  : '1kg siput sedut - cut tip and cleaned, 2 eggs, 5 shallots, 5 cloves garlic, Half an inch of ginger, Dark soy sauce, Salt, seasoning and sugar, Oil for frying', 
      step1        : '1. Roughly pound onion, garlic and ginger',
      step2        : '2. Beat eggs until fluffy, then set aside',
      step3        : '3. Heat oil, sauté onion and ginger that has been pounded',
      step4        : '4. Put in the siput sedut when it became aromatic, mix slowly until water oozes from the siput sedut',
      step5        : '5. After that, put 3-4 tablespoons of dark soy sauce and mix well',
      step6        : '6. Season accordingly, mix well and cook until the gravy becomes more concentrated sauce',
      step7        : '7. When it looked almost thick, add beaten eggs and mix well until the eggs seemed attached to the siput sedut',
      step8        : '8. When the dish is about to dry, remove from heat and ready to be served'   
    },
    {
      id           : 6,
      name         : 'Buttered Prawn',
      img          : 'rs6.jpg',
      ingredients  : '12 medium size prawns, shells on, trimmed and cut off top part of the head, 2 fresh sprigs of curry leaves, 1 teaspoon granulated sugar, 1/2 teaspoon chicken powder, 3 egg yolks, stirred, 3½ oz. (105 g) butter, melted, Frying oil', 
      step1        : '1. In a deep fryer, fry prawns until they turn pink and cooked through. Remove from fryer and set aside',
      step2        : '2. In a wok, combine melted butter and beaten egg yolks. Over high heat, use a ladle to stir the egg yolks in one direction. Keep stirring until the floss',
      step3        : '3. Remove egg mixture from the wok and strain through a strainer. Press the egg mixture against the strainer to get rid of the oil. Set aside',
      step4        : '4. Wash well the wok and put the same oil back into the wok. Fry curry leaves over medium-high heat',
      step5        : '5. Add cooked prawns, granulated sugar and chicken powder. Stir well and add half of egg mixture. Toss well',
      step6        : '6. Remove from the wok. Arrange on serving plate and add remaining egg mixture'  
    },
    {
      id           : 7,
      name         : 'Masak Lemak Cili Api Prawn',
      img          : 'rs7.jpg',
      ingredients  : '300-400g prawns wash with tamarind juice, A bunch of bird eye chilies, 3 shallots, 2 garlic, 1 turmeric leave, 1 lemon grass (bruised), 1 dried tamarind slice or 2 tomatoes (to add sourness), 1 anchovies cube, coconut milk (2 cups or so), salt to taste, Optional: you can add long beans', 
      step1        : '1. Pound bird eye chilies, shallots and garlic',
      step2        : '2. Pour coconut milk in a pot',
      step3        : '3. Add pounded ingredients, lemon grass, and turmeric leave and anchovies stock',
      step4        : '4. Simmer on a slow fire and always stir. Do not leave unstirred or the coconut milk will "separate"',
      step5        : '5. Let simmer',
      step6        : '6. When boiling, this is the time to add the long beans',
      step7        : '7. Then add prawns followed by tamarind slice or tomatoes',
      step8        : '8. Add salt ',
      step9        : '9. Once the prawns have changed color, remove from heat'   
    },
    {
      id           : 8,
      name         : 'Sambal Petai Prawn',
      img          : 'rs8.jpg',
      ingredients  : '80 milliliters / 2&frac35; fl oz vegetable oil, 50 milliliters / 1½ fl oz. tamarind juice - mix tamarind pulp with hot water, strain, 1 tablespoon palm sugar (gula melaka), 40 bitter beans or stink beans (petai), peeled, ½ kilogram / 1 pound of medium-sized prawns, shelled, 6 tablespoons / 3 fl oz sugar, Salt to taste, Paste (to be finely ground): 10 shallots, peeled (red onions), 30 dried chilies, soaked in hot water to soften and drain, 1 tablespoon / ½ fl oz roasted shrimp paste (belacan), 1 cup water', 
      step1        : '1. Heat cooking oil in a wok, add paste and stir-fry till fragrant and oil begins to appear on the surface',
      step2        : '2. Add in bitter beans and fresh prawns. Then pour in tamarind juice and palm sugar',
      step3        : '3. Cook for several minutes, add salt to taste and water to prevent burning',
      step4        : '4. Dish onto a serving plate when prawns are cooked'   
    },
    {
      id           : 9,
      name         : 'Masak Lemak Prawn',
      img          : 'rs9.jpg',
      ingredients  : 'Part I: 15 to 20 dried chilies (cut, discard seeds and soak in hot boiling water for 20 minutes), 10 to 12 shallots4 pips garlic, 2 candlenuts, 2 stalks lemongrass (use the pale part about 4 inches from the base), 1 inch-long turmeric, 1/2 inch-long galangal, 1 tablespoon Asian shrimp paste (belacan), Part II: About 1 liter water, Half of a ripen pineapple (but not too over ripe), cut into 1.5 inch pieces, Part III: 250 ml thick coconut milk, Part IV: 10 to 12 large prawns with shell intact', 
      step1        : '1. Blend all ingredients in Part I to form a paste',
      step2        : '2. Heat 3/4 cup oil in a wok/pan and sauté the paste till it becomes a bit drier and fragrant ',
      step3        : '3. Transfer the sautéed paste to a cooking pot and add Part II',
      step4        : '4. If you find the gravy a bit too thick, you can add a little bit more water',
      step5        : '5. Allow it to simmer for about 15 minutes till the sweetness from the pineapple is released into the gravy',
      step6        : '6. After that, pour in Part III and allow it to simmer to a boil for 2-3 minutes',
      step7        : '7. Then, add in the washed prawns and let it boil till they are cooked',
      step8        : '8. Turn off the heat and serve hot with a plate of rice'  
    },
    {
      id           : 10,
      name         : 'Fried Crab With Salted Egg',
      img          : 'rs10.jpg',
      ingredients  : '2.5lb crab, 6 salty egg yolks, ½ tablespoon salt, A pinch of cornstarch', 
      step1        : '1. Steam salty egg yolks until fully cooked; mash cooked egg yolks and put aside',
      step2        : '2. Clean and chop crab into large pieces; coat the pieces with cornstarch',
      step3        : '3. Deep-fried crab pieces in a generous amount of oil',
      step4        : '4. Put aside when done',
      step5        : '5. Heat a small amount of oil over low heat',
      step6        : '6. Add mashed egg yolk and salt; stir-fry until the mixture bubbles',
      step7        : '7. Add crab pieces to the egg yolks and mix thoroughly'   
    },
    {
      id           : 11,
      name         : 'Crab Soup',
      img          : 'rs11.jpg',
      ingredients  : '4 pieces of crab, 4 red onion (ground), 2 cloves garlic (blended), 1.5 cm ginger (ground), 1 pack of soup bunjut, 1 large onion (sliced), 2 stalks lemongrass, Salt to taste, Celery, Water', 
      step1        : '1. Clean the crab and toss',
      step2        : '2. Fry the grounded ingredients until fragrant',
      step3        : '3. Add water and broth bunjut',
      step4        : '4. After boiling add crab, lemon, onion and salt',
      step5        : '5. Garnish with celery leaves when serving'   
    },
    {
      id           : 12,
      name         : 'Masak Lemak Cili Api Crab',
      img          : 'rs12.jpg',
      ingredients  : '3 fresh crabs, 1 stalk lemongrass, crushed, 1 turmeric leaves, torn, 2 cups coconut milk, 1 slice sliced tamarind, For finely ground materials: 5 chili api, 2 red chilies, 1/2 large onion, 1 inch turmeric, 3 shallots, 3 cloves garlic, Pinch of shrimp paste', 
      step1        : '1. Wash and clean the crab ',
      step2        : '2. Mix crab with finely ground ingredients and cook for about 5 minutes',
      step3        : '3. Then add the coconut milk. Diluted with a little water if the soup is too thick',
      step4        : '4. Slow down the fire and stir so that the milk does not clot',
      step5        : '5. Add tamarind, turmeric, lemon grass and leaves',
      step6        : '6. Put in the salt and sugar accordingly and bring to a boil'  
    },
   {
      id           : 13,
      name         : 'Sambal Crab',
      img          : 'rs13.jpg',
      ingredients  : '6 crabs, 30 sticks of dried chilies (deseeded), 2 large onions, 2 cloves garlic, 1 stalk lemongrass, Salt to taste, Sugar to taste, 1/2 cup cooking oil, 4 cups water, Parsley as garnish', 
      step1        : '1. Blend dried chilies, onion, garlic, lemon grass and add 2 cups of water',
      step2        : '2. Heat the oil and add the chili mixture into the pan',
      step3        : '3. Bring to a partial rise and put in the crab simmer until slowly',
      step4        : '4. When the sauce has more oil, add salt and sugar to taste, stirring until well blended',
      step5        : '5. Turn off the heat and ready to be served' 
    },
    {
      id           : 14,
      name         : 'Sotong Sumbat Pulut',
      img          : 'rs14.jpg',
      ingredients  : '6 squids, 1 cup glutinous rice, 1 cup coconut milk, 2 tablespoons chilies paste, 2 small garlic cloves **, 2 stalks lemongrass **, ** 1/2 inch galangal, 2 buah keras **, ** Blended / mashed fine, 2 cups coconut milk, Salt and sugar to taste, skewers, Oil for frying', 
      step1        : '1. Cook / steamed glutinous rice with coconut milk until half cooked, stuff into the squid, approximately 3/4 of the squid',
      step2        : '2. Then pin it on / off with a skewer',
      step3        : '3. Heat the oil and fry the blended ingredients until fragrant',
      step4        : '4. Add chilies paste and cook until oil breaks',
      step5        : '5. Add coconut milk and squid',
      step6        : '6. Add salt and sugar to taste',
      step7        : '7. Cook over medium heat until the sauce thickens'   
    },
    {
      id           : 15,
      name         : 'Sotong Singgang Kelantan',
      img          : 'rs15.jpg',
      ingredients  : '½ kilograms squid, 3 cloves garlic (cut thinly), 5 shallots (cut thinly), 2 lemongrass, Salt to taste', 
      step1        : '1. Boil squid with water',
      step2        : '2. Add the crushed onion and lemongrass',
      step3        : '3. Add little salt'  
    },
    {
      id           : 16,
      name         : 'Sotong Goreng Kunyit',
      img          : 'rs16.jpg',
      ingredients  : 'Cuttlefish (cut accordingly), 1 large garlic cloves / Holland, 2 shallots / small, 1 tablespoon oyster sauce, 1 tablespoon soy sauce', 
      step1        : '1. Cut squid according to desired size',
      step2        : '2. Clean and strain',
      step3        : '3. Mix squid with little turmeric and salt squid',
      step4        : '4. Half-baked squid to dry the water in inside',
      step5        : '5. Sauté shallot and onion until fragrant.',
      step6        : '6. Add a tablespoon oyster sauce and 1 tablespoon soy sauce, stir well',
      step7        : '7. Add squid when fragrant',
      step8        : '8. Fry until cooked' 
    },
    {
      id           : 17,
      name         : 'Mentarang Bakar',
      img          : 'rs17.jpg',
      ingredients  : 'Mentarang, 5 tablespoon sambal paste, 2 tablespoon oyster sauce, 1 tablespoon sweet soy sauce', 
      step1        : '1. Combine all the above items. Stir well. Season',
      step2        : '2. Take mentarang that have been boiled earlier',
      step3        : '3. Mix in chili paste and put in aluminum foil',
      step4        : '4. Wrap the foil aluminum neatly and place on a non-stick pan',
      step5        : '5. Bake until cooked'  
    },
    {
      id           : 18,
      name         : 'Mentarang Masak Lemak',
      img          : 'rs18.jpg',
      ingredients  : '1 kilo mentarang, 15 chilli padi, 1 inch turmeric, 1 stalk lemongrass,1 cup coconut milk, 1/2 cup coconut milk', 
      step1        : '1. Blend the chilies and turmeric',
      step2        : '2. Pour the blended ingredients in a saucepan',
      step3        : '3. Add lemon grass and coconut milk',
      step4        : '4. Let boil, add mentarang',
      step5        : '5. Once cooked, add coconut milk and salt'  
    },
   {
      id           : 19,
      name         : 'Mentarang Tumis Pedas',
      img          : 'rs19.jpg',
      ingredients  : '1 kg mentarang (washed thoroughly), 1 slice turmeric leaves, 2 stalks lemongrass, Cooking oil to taste, 1 tablespoon fish sauce, 2 tablespoon chili sauce, 2 tablespoon oyster sauce, 2 tablespoon light soy sauce, 1 tablespoon sugar, Salt and pepper to taste, 1 tablespoon tamarind mixed with water, 1 ~ 2 cups water (depending on needs), Minced Ingredient: 1 whole large onion, 3 cloves red onion, 3 cloves garlic, 1 inch ginger, Sliced 2 stalks lemongrass, 10 dried chilies cutted and boiled for awhile, 6 chilies padi', 
      step1        : '1. Heat the oil and fry the minced ingredients and lemongrass. Fry until crisp and fragrant',
      step2        : '2. Enter all sauces and tamarind. Let it boil',
      step3        : '3. Enter mentarang and turmeric leaf. Stir well',
      step4        : '4. Add sugar, seasoning and salt ',
      step5        : '5. Stir to let the sauce thickens '   
    },
    {
      id           : 20,
      name         : 'Spaghetti Olio Seafood',
      img          : 'rs20.jpg',
      ingredients  : 'Spaghetti, 12 medium shrimps/prawns (shells & veins removed), 3 medium squids (cleaned & cut it in ringlets), 100g of mussels, 2 tablespoon minced garlic, 5 tablespoon olive oil, 2 tablespoon dried parsley, 2 tablespoon birds eye chilies / cili padi (optional), 2 tablespoon chili flakes (optional), Salt', 
      step1        : '1. Add some olive oil and salt to a pot of water, and boil the pasta until drain and set aside',
      step2        : '2. Heat olive oil in a pan & stir fry the minced garlic under low heat till golden brown',
      step3        : '3. Add in the seafood (be careful not to overcook)',
      step4        : '4. Toss in the pasta and stir evenly',
      step5        : '5. Season with salt, chili flakes, birds eye chilies and stir well',
      step6        : '6. Garnish with dried parsley before serving'   
    },
    {
      id           : 21,
      name         : 'Prawn Fettuccine',
      img          : 'rs21.jpg',
      ingredients  : '2 tablespoon chopped fresh parsley, ½ cup olives, pitted and halved, 500g pasta Sauce, 1 tablespoon finely grated lemon rind, 1 clove garlic, crushed, 200g medium cooked prawns, peeled & deveined, 2 tablespoon chopped fresh basil', 
      step1        : '1. Cook pasta according to packet directions. Drain',
      step2        : '2. Place sauce in saucepan and bring to simmer over medium heat. Add olives, lemon rind & garlic. Cover & simmer for 5 minutes',
      step3        : '3. Add prawns, basil & parsley. Cook for 1 minute. Remove sauce from heat',
      step4        : '4. Divide San Remo pasta among bowls. Top with pasta sauce. Serve immediately'  
    },
    {
      id           : 22,
      name         : 'Shell Out',
      img          : 'rs22.jpg',
      ingredients  : '500 gram shrimp - shrimp medium to large size, 500 kg of crabs, 300 gram squid, 300 gram clam @ lala, 10 dried chilies - boiled and blend, 7 cloves garlic - chopped @ pounded, 1/2 inch ginger - finely sliced, 2 large shallots - sliced, 1 Holland onion - diced, 1/2 inch shrimp paste - pounded, 1 chicken stock cube, 2 tablespoon oyster sauce, 1 tablespoon ground black pepper, 2 tablespoons butter, 1/2 tablespoon curry powder, 2 slices of lemon, Salt and sugar to taste', 
      step1        : '1. Boil seafood in hot water for 4 to 5 minutes. Ensure the water is boiling before putting in seafood',
      step2        : '2. Put butter, a little oil in a pan. Add garlic, ginger and onion. Cook until fragrant',
      step3        : '3. Add dry chili paste that has the blend with a little water - cook oil breaks',
      step4        : '4. Add shrimp paste, chicken stock cubes, curry powder, oyster sauce, black pepper, roasted Holland onion, curry leaves, salt and sugar to taste. Stir for a while',
      step5        : '5. Put all the seafood. Put lemon slices and cook 3-5 minutes ' 
    }
    );

    localStorage.setItem("recipeJson",JSON.stringify(items));
})

.controller('PlaylistsCtrl', function($scope) {
  $scope.playlists = [
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
  ];
})

.controller('HomeCtrl', function($scope){  

$scope.options = {
  loop: true,
  effect: 'slide',
  speed: 500,
  autoplay: 3000,
  autoplayDisableOnInteraction: false,
  initialSlide: 0,
}
})

.controller('SelectCtrl', function($scope, $state){

  $scope.goTo = function(place)
  {
    if (place == 1)
    {
      $state.go('app.sekinchan');
    }

    if (place == 2)
    {
      $state.go('app.sungai');
    }

    if (place == 3)
    {
      $state.go('app.sabak');
    }

  };

})

.controller('searchController', function($scope, $state)
{

    //Specify TIcketDara for each category
        if ($scope.$root.searchForStatus == "myTicket")
        {
            //$scope.ticketData = JSON.parse(localStorage.getItem('myTicketOffline'));
        }
        else if (($scope.$root.searchForStatus == "openTicket"))
        {
            //$scope.ticketData = JSON.parse(localStorage.getItem('openTicketOffline'));
        }
        else if (($scope.$root.searchForStatus == "answeredTicket"))
        {
            //$scope.ticketData = JSON.parse(localStorage.getItem('answeredTicketOffline'));
        }
        else if (($scope.$root.searchForStatus == "overdueTicket"))
        {
            //$scope.ticketData = JSON.parse(localStorage.getItem('overdueTicketOffline'));
        }
        else
        {
            //$scope.ticketData = JSON.parse(localStorage.getItem('closedTicketOffline'));
        }

       // $scope.searchKey = $scope.$root.searchFor;

    // //Ticket Details
    // $scope.goDetails = function(ID) {
    //     $scope.$root.current_ticket_id = ID;
    //     $state.go('tab2.goDetails');
    // };
})

.controller('SekinchanCtrl', function($scope, $ionicLoading, $compile, $state) {
  $scope.initialize = function() {
    var myLatlng = new google.maps.LatLng(3.506235, 101.102214);
    
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  

  //list restaurant
  $scope.openList = function() {
    $state.go('app.sekinchanRes');
  }

})


.controller('SabakCtrl', function($scope, $ionicLoading, $compile, $state) {
  $scope.initialize = function() {
    var myLatlng = new google.maps.LatLng(3.769210, 100.983993);
    
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  

      //list restaurant
  $scope.openList = function() {
    $state.go('app.sabakRes');
  }
})

.controller('SungaiCtrl', function($scope, $ionicLoading, $compile, $state) {
  $scope.initialize = function() {
    var myLatlng = new google.maps.LatLng(3.674244, 100.990081);
    
    var mapOptions = {
      center: myLatlng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("map"),
        mapOptions);


    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title: 'Uluru (Ayers Rock)'
    });

    google.maps.event.addListener(marker, 'click', function() {
      infowindow.open(map,marker);
    });

    $scope.map = map;
  }  

    //list restaurant
  $scope.openList = function() {
    $state.go('app.sungaiRes');
  }
})

.controller('SekinchanResCtrl', function($scope, $ionicLoading, $compile, $state) {
  $scope.items = [{
      name   : 'Loong Hua Seafood Restaurant (Non Muslim)',
      address: '73, Lorong Tiga, Pekan Sekinchan, 45400 Sekinchan, Selangor, Malaysia (3 Star)',
      phone  : '+60 3-3241 0651',
      hours  : '11AM–10PM',
      others : 'Chinese New Year might affect these hours',
      img    : 'img/sk1.png'
    },{
      name   : 'Restoran Guan Seng Long Sdn Bhd (Non Muslim)',
      address: 'Taman Sekinchan Damai, 45400 Sekinchan, Selangor, Malaysia (3 Star Lebih)',
      phone  : '+60 3-3241 8494',
      img    : 'img/sk2.png'
    },{
      name   : 'Hao Xiang Chi Seafood Sdn Bhd (Non Muslim)',
      address: 'Jalan Gereja, Pekan Sekinchan, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 3-3241 3188',
      img    : 'img/sk3.png'
    },{
      name   : 'Restoran sheng hui(4 star) (Non Muslim)',
      address: 'Address: 283, Jalan Pasar, Pekan Sekinchan, 45400 Sekinchan, Selangor, Malaysia',
      img    : 'img/sk4.png',
      hours  : 'Monday - Sunday (12PM - 12AM)'
    },{
      name   : 'Wong Corner Homestay And Restaurant ( 4 star) (Non Muslim)',
      address: '45 jalan bernam site C, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 17-685 6018',
      hours  : '7:30PM–12AM',
      others : 'Chinese New Year might affect these hours',
      img    : 'img/sk5.png'
    },{
      name   : 'Guan Seng Long Seafood (Non Muslim)',
      address: 'Lot P.T. 2-C, Jalan Mentari, Pekan Sekinchan, 45400 Sekinchan, Selangor, Malaysia'
    },{
      name   : 'Beng Kee Seafood Restaurant (Non Muslim)',
      address: 'Jalan Gereja, Pekan Sekinchan, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 3-3241 0598'
    },{
      name   : 'Restoran Cha Po Tion (Non Muslim)',
      address: '9990B Jalan Besar Bagan, Bagan Sekinchan, 45400 Sekinchan, Selangor',
      phone  : '+60 16-676 1769',
      hours  : '7AM–2PM',
      others : 'Chinese New Year might affect these hours ',
      img    : 'img/sk6.png'
    },{
      name   : '15 Redang Station (4 star setengah) (Non Muslim)',
      address: '291, Lorong Sebelas, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 19-644 7494',
      img    : 'img/sk7.png'
    },{
      name   : '潮人海鲜火锅 ChaoRen Seafood Steamboat (Non Muslim)',
      address: '125, Jalan Bagan, Bagan Sekinchan, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 16-303 0313',
      hours  : '3PM–12AM',
    },{
      name   : 'Guan Hwat Restaurant (Non Muslim)',
      address: 'Lot 2942 Jalan Bagan, Sekinchan 45400, Malaysia',
      phone  : '+60 16-688 2609'
    },{
      name   : 'Restoran Kun Kee (Non Muslim)',
      address: '42 Jalan Sekinchan Damai 1 | Taman Sekinchan, Sekinchan 45400, Malaysia',
      phone  : '+60 16-379 3638',
      img    : 'img/sk8.png'
    },{
      name   : 'Restoren Wan Lau (Non Muslim)',
      address: '176 Bagan Sekinchan, Sekinchan 45400, Malaysia',
      phone  : '+60 14-669 3123',
      img    : 'img/sk9.png'
    },{
      name   : 'Restoran Bagan Sekinchan (Non Muslim)',
      address: ': No. 168-C Jalan Bagan | Lorong 5, Sekinchan 45400, Malaysia',
      phone  : '+60 16-279 5608',
      img    : 'img/sk10.png'     
    },{
      name   : 'Restoran Loong Hua (Non Muslim)',
      address: 'Lorong 3 | No 73, Sekinchan 45400, Malaysia',
      phone  : '+60 3-3241 0651',
      img    : 'img/sk11.png' ,
      hours  : 'Monday Closed, Tuesday - Sunday (11AM - 10PM)'
    },{
      name   : 'Aji Mentrang Bakar ',
      address: 'Kampung Sungai Leman, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '017-2756817 ',
      img    : 'img/sk12.png' ,
      hours  : 'Wednesday Closed, Thursday - Tuesday (11AM - 10PM)'
    },{
      name   : 'Faridah Mentarang Bakar',
      address: 'Pasir Panjang, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 16-696 5982',
      img    : 'img/sk13.png' 
    }
    ];

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleItem= function(item) {
    if ($scope.isItemShown(item)) {
      $scope.shownItem = null;
    } else {
      $scope.shownItem = item;
    }
  };
  $scope.isItemShown = function(item) {
    return $scope.shownItem === item;
  };

  $scope.goNavigate = function()
  {
    launchnavigator.navigate("London, UK");
    console.log("navigate");
  }

})

.controller('SungaiResCtrl', function($scope, $ionicLoading, $compile, $state) {
  $scope.items = [{
      name   : 'Yaw Kee Seafood Restaurant (Non Muslim)',
      address: '45300, 3, Jalan Menteri, Taman Berjaya, 45200 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 3-3224 3036',
      img    : 'img/sg1.png'
  },{
      name   : 'Long Seng Enterprise (M) Sdn. Bhd. (Non Muslim)',
      address: 'No. 8, Jalan 4, Bagan Sungai Besar, Selangor, 45300 Sungai Besar, Malaysia',
      phone  : '60 3-3224 2360',
  },{
      name  :  'Siti Wahida Seafood (Muslim)',
      address: 'Sungai Besar, 45400 Sungai Besar, Selangor, Malaysia',
      img    : 'img/sg2.png'  
  },{
      name   : 'Chong Thai Enterprise (Non Muslim)',
      address: '45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 3-3224 3390'    
  },{
      name   : 'Restaurant Xin Shun Kou Fu Seafood (Non Muslim)',
      address: '121-b, Jalan Panchang Bedena, Bagan Sungai Besar, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : ':+60 3-3224 1599',
      img    : 'img/sg3.png'   
  },{
      name   : 'Mutiara Seafood (Muslim)',
      address: 'Sungai Besar, 45300 Sungai Besar',
      phone  : '017-206 369',
      img    : 'img/sg4.png' 
  },{
      name   : 'Steamboat Ak-Rushd Steamboat And Grills',
      address: 'Jalan Bunga Orkid, Sungai Besar Sentral 45300 Sungai Besar',
      phone  : '012-235 9238 ',
      img    : 'img/sg5.png',
      hours  : '4.00PM - 12.00AM'
  }];

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleItem= function(item) {
    if ($scope.isItemShown(item)) {
      $scope.shownItem = null;
    } else {
      $scope.shownItem = item;
    }
  };
  $scope.isItemShown = function(item) {
    return $scope.shownItem === item;
  };

})

.controller('SabakResCtrl', function($scope, $ionicLoading, $compile, $state) {
  $scope.items = [{
      name   : 'Restaurant Makanan Laut Tien Yin (Non Muslim)',
      address: 'Kampung Sungai Gatal, 45400 Sabak, Selangor, Malaysia',
      hours  : '12–10:30PM',
      others : 'Chinese New Year might affect these hours',
      img    : 'img/sbk2.png' 
  },{
      name   : 'Mee Udang Sabak Bernam, BNO',
      hours  : '11AM - 7PM',
      phone  : '016-6970517',
      img    : 'img/sbk1.png' 
  }];

  /*
   * if given group is the selected group, deselect it
   * else, select the given group
   */
  $scope.toggleItem= function(item) {
    if ($scope.isItemShown(item)) {
      $scope.shownItem = null;
    } else {
      $scope.shownItem = item;
    }
  };
  $scope.isItemShown = function(item) {
    return $scope.shownItem === item;
  };

})

.controller('RecipeCtrl', function($scope, $ionicLoading, $compile, $state) {

  $scope.items =  JSON.parse(localStorage.getItem('recipeJson'));

  console.log($scope.items);

  $scope.recipeDetails = function(id)
  {
    $scope.$root.rcp_id = id;
    $state.go('app.recipeDetails');
  };

  $scope.goCredits = function()
  {
    $state.go('app.credit');
  }

})

.controller('RecipeDetailsCtrl', function($scope, $ionicLoading, $compile, $state) {

  $scope.items =  JSON.parse(localStorage.getItem('recipeJson'));
  
  var rcpID = $scope.$root.rcp_id;

  var recipeDetails = $scope.items;

            angular.forEach(recipeDetails, function(value)
            {
                if (value.id == rcpID)
                {
                    this.push(value);
                    $scope.currentRecipe = value;
                }
            }, recipeDetails);  

  console.log($scope.currentRecipe);

})

.controller('VideoCtrl', function($scope, $ionicLoading, $compile, $state, $sce) {

   $scope.trustSrc = function(src) {
    return $sce.trustAsResourceUrl(src);
  }

})

.controller('MarketCtrl', function($scope, $ionicLoading, $compile, $state, $sce) {

  $scope.toggleItem= function(item) {
    if ($scope.isItemShown(item)) {
      $scope.shownItem = null;
    } else {
      $scope.shownItem = item;
    }
  };
  $scope.isItemShown = function(item) {
    return $scope.shownItem === item;
  };

})

.controller('WhereCtrl', function($scope, $ionicLoading, $compile, $state, $sce, $ionicSlideBoxDelegate ) {

  $scope.sekinchan = [{
      name   : 'A plus boutique Hotel',
      address: 'No 6 Jalan Perdana 2, Pekan Sekinchan, 45400 Sekinchan, Selangor, Malaysia ',
      phone  : '+60 3-3241 5555',
      img1   :  "img/wsk1.png",
      img2   :  "img/wsk2.png"      
  },{
      name   : 'East Sun Hotel',
      address: '6, Jalan Indah 2/5, Taman Indah 2, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 11-1261 6908',
      img1   :  "img/wsk3.png",
      img2   :  "img/wsk4.png"  
  },{
      name  : 'Farm Ville Cafe & Homestay',
      phone  : '+60 17-206 8666',
      address: '11304, Jalan Tepi Sawah, Sekinchan, 45400 Sekinchan, Selangor, Malaysia',
      img1   :  "img/wsk31.png",
      img2   :  "img/wsk32.png"    
  },{
      name   : 'GOLDEN HARVEST HOTEL',
      address: 'Jalan Menteri, Sekinchan, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 3-3241 0075',
      img1   :  "img/wsk41.png",
      img2   :  "img/wsk42.png"      
  },{
      name   : 'Padi Box',
      address: 'Jalan Tali Air 4, Kampung Parit Empat, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 19-320 5688',
      img1   :  "img/wsk51.png",
      img2   :  "img/wsk52.png"     
  },{
      name   : 'Sekinchan Summer Resort',
      address: 'Lot,14533, Kampung Kian Sit, Site A, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 12-245 4111',
      img1   :  "img/wsk61.png",
      img2   :  "img/wsk62.png"   
  }
  ,{
      name   : 'Paddy Village Hotel',
      address: '7, Jalan Bernam, Pekan Sekinchan, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 3-3241 0636',
      img1   :  "img/wsk71.png",
      img2   :  "img/wsk72.png"   
  }
  ,{
      name   : 'House Of Cartoons',
      address: 'Taman Sekinchan Damai, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 13-208 5555',
      img1   :  "img/wsk81.png",
      img2   :  "img/wsk82.png"   
  }
  ,{
      name   : 'Paddy View Homestay And Chalet',
      address: 'Lot 1208 6, Jalan Tepi Sawah, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 19-236 5430',
      img1   :  "img/wsk91.png",
      img2   :  "img/wsk92.png"   
  }
  ,{
      name   : 'D Menara Bayu Homestay',
      address: 'Jalan Sungai Labu, Kampung Sungai Leman, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 13-623 2276',
      img1   :  "img/wsk101.png",
      img2   :  "img/wsk102.png"   
  }
  ,{
      name   : 'Homestay Cik Nor Sekinchan',
      address: 'homestay cik nor sekinchan, ban 2, Jalan Parit 7, 45400 Sekinchan, Selangor, Malaysia',
      phone  : '+60 19-452 3326',
      img1   :  "img/wsk111.png",
      img2   :  "img/wsk112.png"   
  }];

    $scope.sungai = [{
      name   : 'Green Hotel Sungai Besar',
      address: '45300, 20-28, Jalan Ssbc 3, Sungai Besar, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 3-3224 6699',
      img1   :  "img/wsb11.png",
      img2   :  "img/wsb12.png"  
  },{
      name   : 'Chalet Dachya Stay',
      address: 'Kampung Sungai Haji Durani, 45400 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 17-668 5177',
      img1   :  "img/wsb21.png",
      img2   :  "img/wsb22.png"  
  },{
      name  : 'Ocean Hotel Sungai Besar',
      phone  : '21B;;23B, Jalan Panchang Bedena, Bagan Sungai Besar, 45300 Sungai Besar, Selangor, Malaysia',
      address: '+60 11-3749 8138',
      img1   :  "img/wsb31.png",
      img2   :  "img/wsb32.png"  
  },{
      name   : 'Hotel Grand Court Inn',
      address: '1, Jalan 2, Taman Berkat, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 3-3224 1928',
      img1   :  "img/wsb41.png",
      img2   :  "img/wsb42.png"    
  },{
      name   : 'Hotel KOMP1T Sungai Besar',
      address: 'Hotel KOMP1T, Wisma Haji Bahari, Jalan Persekutuan, 5, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 3-3224 1333',
      img1   :  "img/wsb51.png",
      img2   :  "img/wsb52.png"   
  },{
      name   : 'Dorani Bayu Resort & Service Sdn. Bhd.',
      address: 'D Muara Dorani Marine Parks, Jalan Lama Kuala Selangor, 45300, Sungai Besar, Selangor, 45300, Malaysia',
      phone  : '+60 3-3224 6800',
      img1   :  "img/wsb61.png",
      img2   :  "img/wsb62.png" 
  }
  ,{
      name   : 'Shah Jehan House Stay',
      address: 'Lot 350, Jalan Besar, Kampung Sungai Limau, Selangor, 45300 Sungai Besar, Malaysia',
      phone  : '+60 13-201 0062',
      img1   :  "img/wsb81.png",
      img2   :  "img/wsb82.png" 
  }
  ,{
      name   : 'Homestay Safa',
      address: 'Kampung Sungai Limau, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 12-522 7958',
      img1   :  "img/wsb91.png",
      img2   :  "img/wsb92.png" 
  }
  ,{
      name   : 'D sawah Bendang Homestay',
      address: 'Jalan Parit 6 Timur, Parit 7 Timur, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 12-808 4818',
      img1   :  "img/wsb101.png",
      img2   :  "img/wsb102.png" 
  }
  ,{
      name   : 'Homestay JKKK Sungai Haji Dorani, Sungai Besar, Selangor',
      address: '5, Kampung Sungai Haji Durani, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 19-911 4470',
      img1   :  "img/wsb111.png",
      img2   :  "img/wsb112.png" 
  }
  ,{
      name   : 'HOMESTAY DAMAI RASIDIN VILLA',
      address: 'LOT 1107,TEBUK HAJI ROS JALAN RASIDIN,45300 SUNGAI BESAR SELANGOR, Jalan Pantai 3 Tebuk Rasidin, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 13-399 0906',
      img1   :  "img/wsb121.png",
      img2   :  "img/wsb122.png" 
  }
  ,{
      name   : 'Sg. Hj. Dorani Homestay',
      address: 'No. 3, Pekan Selasa Lama, Sungai Haji Dorani, Selangor, 45300 Sungai Besar, Malaysia',
      phone  : '+60 13-607 7025',
      img1   :  "img/wsb131.png",
      img2   :  "img/wsb132.png" 
  }
  ,{
      name   : 'Adira Homestay',
      address: ': 5, Jalan Purnama 1, Taman Purnama, 45300 Sungai Besar, Selangor, Malaysia',
      phone  : '+60 13-289 281',
      img1   :  "img/wsb141.png",
      img2   :  "img/wsb142.png" 
  }];

 $scope.sabak = [{
      name   : 'Sri Bernam Hotel',
      address: 'Taman Berjaya, 45200 Sabak, Selangor, Malaysia',
      phone  : '+60 3-3216 4033',
      img1   :  "img/wsn11.png",
      img2   :  "img/wsn12.png"
  },{
      name   : 'Hay Resort N Services',
      address: 'Lot 26, Jalan Rumah Rehat, Rumah Rehat Kerajaan, 45200 Sabak, Selangor, Malaysia',
      phone  : '+60 3-3216 2187',
      img1   :  "img/wsn21.png",
      img2   :  "img/wsn22.png"
  },{
      name  : 'Homestay Air Manis',
      phone  : '+60 3-3216 1696',
      address: 'no.30, jln hj mansor,batu 37 darat,, 45200, Selangor, Malaysia',
      img1   :  "img/wsn31.png",
      img2   :  "img/wsn32.png"  
  },{
      name   : 'Zamita Resort, Sabak Bernam, Selangor.',
      address: 'Pekan Sabak Bernam, 45400 Sabak, Selangor, Malaysia',
      phone  : '+60 13-222 2131',
      img1   :  "img/wsn41.png",
      img2   :  "img/wsn42.png"    
  },{
      name   : 'teratak umi chalet',
      address: 'A118, Kampung Banting, 45200 Sabak, Malaysia',
      phone  : '+60 19-284 6550',
      img1   :  "img/wsn51.png",
      img2   :  "img/wsn52.png"   
  },{
      name   : 'Homestay Wawasan',
      address: 'No. 49, Jalan Wawasan 1/1, Taman Wawasan, 45200, Sabak Bernam, Selangor, 45200 Sabak, Malaysia',
      phone  : '+60 16-979 9366',
      img1   :  "img/wsn61.png",
      img2   :  "img/wsn62.png" 
  }];

  
  $scope.slideVisible = function(index){
    if(  index < $ionicSlideBoxDelegate.currentIndex() -1 
       || index > $ionicSlideBoxDelegate.currentIndex() + 1){
      return false;
    }
    
    return true;
  }
  
 

});









